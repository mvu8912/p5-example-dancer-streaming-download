package StreamDownload;
use Dancer ":syntax";

our $VERSION = "0.1";

get "/feed/:partner_name" => sub {
    my $partner_name = param "partner_name";
    return send_file(
        \"feed.xml",
        streaming => 1,
        callbacks => { override => \&_streaming_response }
    );
};

sub _streaming_response {
    my $response = shift;
    my $output   = $response->(
        [ 200, [ "Content-Disposition" => q{attachment; filename="feed.xml"} ]]
    );

    my $total_jobs = _count();

    $output->write( template feed_header => { total => $total_jobs } );

    foreach my $id ( 1 .. _count() ) {
        $output->write(
            template feed_job => {
                job_id      => $id,
                description => rand() x 100
            }
        );
    }

    $output->write( template "feed_footer" );
}

sub _count {
    return 650;
}

true;
